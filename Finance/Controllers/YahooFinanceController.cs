﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Finance.Models;
using Finance.Services;
using Microsoft.AspNetCore.Mvc;

namespace Finance.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class YahooFinanceController : ControllerBase
    {
        private readonly IMarket _market;
        public YahooFinanceController(IMarket market)
        {
            _market = market;
        }

        [HttpGet]
        public Dictionary<DateTime, IEnumerable<Performance>> GetChartComparisons([FromQuery]Comparison comparison)
        {
            return _market.GetPerformance(comparison);
        }
    }
}