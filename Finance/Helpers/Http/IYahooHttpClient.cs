﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Finance.Helpers
{
    public interface IYahooHttpClient
    {
        /// <summary>
        /// Wrapper under HttpClient.GetAsync().
        /// </summary>
        /// <param name="url">Url segments. Example: /some/addr .</param>
        /// <returns>Response from HttpClient.</returns>
        Task<HttpResponseMessage> GetAsync(string url);
    }
}