﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Finance.Models;
using Microsoft.Extensions.Configuration;

namespace Finance.Helpers
{
    public class YahooHttpClient : IYahooHttpClient
    {
        private readonly HttpClient _client;
        
        public YahooHttpClient(HttpClient client, IConfiguration configuration)
        {
            _client = client;
            _client.BaseAddress = new Uri(configuration.GetValue<string>("AppSettings:YahooFinanceUrl"));
            _client.DefaultRequestHeaders.Add(YahooHeaders.ApiKey, configuration.GetValue<string>("AppSettings:YahooApiKey"));
        }
        
        public async Task<HttpResponseMessage> GetAsync(string url)
        {
            return await _client.GetAsync(url);
        }
    }
}