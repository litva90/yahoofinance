﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Finance.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChartValues",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Symbol = table.Column<string>(type: "TEXT", nullable: true),
                    When = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Interval = table.Column<string>(type: "TEXT", nullable: true),
                    High = table.Column<double>(type: "REAL", nullable: false),
                    Low = table.Column<double>(type: "REAL", nullable: false),
                    Open = table.Column<double>(type: "REAL", nullable: false),
                    Close = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChartValues", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrackingStocks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    StockName = table.Column<string>(type: "TEXT", nullable: true),
                    LastSavedOn = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrackingStocks", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChartValues_Symbol_Interval_When",
                table: "ChartValues",
                columns: new[] { "Symbol", "Interval", "When" },
                unique: true);
            
            migrationBuilder.InsertData(
                table: "TrackingStocks",
                columns: new[] { "StockName" },
                values: new object[,]
                {
                    { "msft" },
                    { "wdc" },
                    { "spy" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChartValues");

            migrationBuilder.DropTable(
                name: "TrackingStocks");
        }
    }
}
