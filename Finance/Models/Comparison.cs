﻿namespace Finance.Models
{
    public class Comparison
    {
        public string FirstSymbol { get; set; }
        public string SecondSymbol { get; set; }
        public IntervalEnum Interval { get; set; }
        public RangeEnum Range { get; set; }

    }
}