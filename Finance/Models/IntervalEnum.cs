﻿namespace Finance.Models
{
    public enum IntervalEnum
    {
        Hour = 3600,
        Day = 86400
    }
}