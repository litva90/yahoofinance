﻿using System;

namespace Finance.Models
{
    public class Performance
    {
        public string Symbol { get; set; }
        public double Percentage { get; set; }
    }
}