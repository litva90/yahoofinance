﻿using System.Collections.Generic;

namespace Finance.Models
{
    public class StockData
    {
        public string Symbol { get; set; }
        public int[] Timestamps { get; set; }
        public string Interval { get; set; }
        public SymbolIndicators Indicators { get; set; }
        public StockData Comparison { get; set; }
    }
}