﻿using System.ComponentModel.DataAnnotations;

namespace Finance.Models
{
    public class StockParameter
    {
        /// <summary>
        /// The character you are looking for.
        /// </summary>
        public string Symbol { get; set; }
        public string Interval { get; set; }
        public string Range { get; set; }
        public string Region { get; set; }
        public string Comparisons { get; set; }
    }
}