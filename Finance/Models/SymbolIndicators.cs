﻿using System.Collections.Generic;

namespace Finance.Models
{
    public class SymbolIndicators
    {
        public double[] High { get; init; }
        public double[] Low { get; init; }
        public double[] Open { get; init; }
        public double[] Close { get; init; }
    }
}