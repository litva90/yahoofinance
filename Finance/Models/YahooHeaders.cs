﻿namespace Finance.Models
{
    public static class YahooHeaders
    {
        /// <summary>
        /// Api key header for Yahoo finance. 
        /// </summary>
        public const string ApiKey = "X-RapidAPI-Key";
    }
}