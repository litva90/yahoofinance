﻿namespace Finance.Repository.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}