﻿using System;

namespace Finance.Repository.Entities
{
    public class ChartValue : BaseEntity
    {
        public string Symbol { get; set; }
        public DateTime When { get; set; }
        public string Interval { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Open { get; set; }
        public double Close { get; set; }
    }
}