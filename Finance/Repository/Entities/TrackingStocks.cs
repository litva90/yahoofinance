﻿using System;

namespace Finance.Repository.Entities
{
    public class TrackingStock : BaseEntity
    {
        public string StockName { get; set; }
        public DateTime? LastSavedOn { get; set; }
    }
}