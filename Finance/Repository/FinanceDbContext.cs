﻿using Finance.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Finance.Repository
{
    public class FinanceDbContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public DbSet<ChartValue> ChartValues { get; set; }
        public DbSet<TrackingStock> TrackingStocks { get; set; }

        public FinanceDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conn = _configuration.GetConnectionString("FinanceDbConnection");
            optionsBuilder.UseSqlite(conn);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChartValue>()
                .HasIndex(cv => new { cv.Symbol, cv.Interval, cv.When })
                .IsUnique(true);
        }
    }
    
    
}