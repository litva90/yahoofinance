﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Finance.Models;

namespace Finance.Services
{
    public interface IMarket
    {

        Task SaveTrackingCharts();

        Dictionary<DateTime, IEnumerable<Performance>> GetPerformance(Comparison comparison);
    }
}