﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finance.Helpers;
using Finance.Models;
using Finance.Repository;
using Finance.Repository.Entities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Finance.Services
{
    public class Market : IMarket
    {
        private readonly IYahooHttpClient _yahooHttpClient;
        private readonly FinanceDbContext _financeContext;
        private readonly ILogger _logger;

        public Market(
            IYahooHttpClient yahooHttpClient, 
            FinanceDbContext financeContext,
            ILogger<Market> logger)
        {
            _yahooHttpClient = yahooHttpClient;
            _financeContext = financeContext;
            _logger = logger;
        }
        
        private async Task<List<ChartValue>> GetChart(StockParameter chartParameter)
        {
            var response = await _yahooHttpClient.GetAsync($"market/get-charts?" +
                                                           $"symbol={chartParameter.Symbol}" +
                                                               $"&region={chartParameter.Region}" +
                                                                                $"&interval={chartParameter.Interval}" +
                                                           $"&range={chartParameter.Range}");

            var content = await response.Content.ReadAsStringAsync();

            var preparingChart = JsonConvert.DeserializeObject<JObject>(content);
            if (preparingChart?["chart"]?["error"]?.Value<JObject>() != null)
            {
                string errorMessage =
                    $"Exception occured. Code: {preparingChart["chart"]?["error"]?["code"]?.Value<string>()}. Message: {preparingChart["chart"]?["error"]?["description"]?.Value<string>()}";
                _logger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }
            
            JObject indicator = preparingChart?["chart"]?["result"]?[0]?["indicators"]?["quote"]?[0]?.Value<JObject>();
            int[] utcTimestamps = preparingChart?["chart"]?["result"]?[0]?["timestamp"]?.Values<int>().ToArray();

            StockData stockData = new StockData
            {
                Symbol = preparingChart?["chart"]?["result"]?[0]?["meta"]?["symbol"]?.Value<string>(),
                Timestamps = utcTimestamps,
                Interval = preparingChart?["chart"]?["result"]?[0]?["meta"]?["dataGranularity"]?.Value<string>(),
                Indicators = new SymbolIndicators
                {
                    Open = indicator?["open"]?.Values<double>().ToArray(),
                    Close = indicator?["close"]?.Values<double>().ToArray(),
                    Low = indicator?["low"]?.Values<double>().ToArray(),
                    High = indicator?["high"]?.Values<double>().ToArray()
                }
            };
            
            return GetChartValuesFromStock(stockData);
        }
        
        public async Task SaveTrackingCharts()
        {
            var allTrackingStocks = _financeContext.TrackingStocks.ToArray();

            foreach (var trackingStock in allTrackingStocks)
            {
                var chartForSaving = await GetChart(new StockParameter()
                {
                    Region = "us",
                    Interval = "15m",
                    Range = "5d",
                    Symbol = trackingStock.StockName
                });
                var onlyNewCharts = chartForSaving
                    .Where(c => c.When > (trackingStock.LastSavedOn ?? DateTime.MinValue))
                    .ToArray();
                _financeContext.ChartValues.AddRange(onlyNewCharts);
                await _financeContext.SaveChangesAsync();

                _financeContext.Attach(trackingStock);
                trackingStock.LastSavedOn = chartForSaving.Max(c => c.When);
                await _financeContext.SaveChangesAsync();
            }
        }
        
        private List<ChartValue> GetChartValuesFromStock(StockData stockData)
        {
            var chartValues = new List<ChartValue>();

            for (int i = 0; i < stockData.Timestamps.Count(); i++)
            {
                chartValues.Add(new ChartValue()
                {
                    Symbol = stockData.Symbol,
                    Open = stockData.Indicators.Open[i],
                    Close = stockData.Indicators.Close[i],
                    High = stockData.Indicators.High[i],
                    Low = stockData.Indicators.Low[i],
                    Interval = stockData.Interval,
                    When = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                        .AddSeconds(stockData.Timestamps[i])
                });
            }

            return chartValues;
        }

        public Dictionary<DateTime, IEnumerable<Performance>> GetPerformance(Comparison comparison)
        {
            var comparingValues = GetChartsOnDate(comparison);
            
            Dictionary<string, double> pricesOnStart = comparingValues
                .GroupBy(v => v.Symbol)
                .ToDictionary(
                    d => d.Key.ToLower(),
                    d => d.Where(cv => cv.Symbol.ToLower() == d.Key.ToLower())
                        .OrderBy(cv => cv.When).First().Open);

            return comparingValues
                    .GroupBy(v => v.When)
                    .ToDictionary(d =>
                            d.Key,
                        d => d.Select(v => new Performance
                        {
                            Symbol = v.Symbol,
                            Percentage = (v.Open - pricesOnStart[v.Symbol.ToLower()]) / pricesOnStart[v.Symbol.ToLower()] * 100
                        })
                    );
        }

        private ChartValue[] GetChartsOnDate(Comparison comparison)
        {
            DateTime chartsSinceDate;
            switch (comparison.Range)
            {
                case RangeEnum.Day:
                    chartsSinceDate = DateTime.Now.AddDays(-1);
                    break;
                case RangeEnum.Week:
                    chartsSinceDate = DateTime.Now.AddDays(-7);
                    break;
                default:
                    chartsSinceDate = DateTime.Now;
                    break;
            }

            return _financeContext.ChartValues
                .Where(cv =>
                    (cv.Symbol.ToLower() == comparison.FirstSymbol.ToLower() ||
                     cv.Symbol.ToLower() == comparison.SecondSymbol.ToLower()) &&
                    chartsSinceDate.Date <= cv.When.Date)
                .OrderBy(cv => cv.When)
                .AsEnumerable()
                .Select(cv => new
                {
                    cv,
                    cv.Symbol,
                    IntervalSplit = ((DateTimeOffset) cv.When).ToUnixTimeSeconds() / (int) comparison.Interval
                })
                .GroupBy(model => new {model.IntervalSplit, model.Symbol})
                .Select(g => g.First())
                .Select(model => new ChartValue()
                {
                    Symbol = model.Symbol,
                    When = model.cv.When,
                    Open = model.cv.Open
                }).ToArray();
        }
    }
}