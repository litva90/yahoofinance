﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Finance.Services
{
    public class ReceiveStockDataHostedService : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private Timer _timer;

        private IServiceProvider _services;
        
        public ReceiveStockDataHostedService(
            IServiceProvider services, 
            ILogger<ReceiveStockDataHostedService> logger)
        {
            _services = services;
            _logger = logger;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is starting.");

            _timer = new Timer(SaveStockData, null, TimeSpan.Zero, 
                TimeSpan.FromMinutes(5));

            return Task.CompletedTask;
        }
        
        private void SaveStockData(object state)
        {
            _logger.LogInformation("Timed Background Service is working.");

            using var scope = _services.CreateScope();
            var marketService = scope.ServiceProvider
                    .GetRequiredService<IMarket>();
            marketService.SaveTrackingCharts().Wait();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Background Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}